package cz.muni.fi.sitola.stanko.sage2.PointParserServer.app;

import cz.muni.fi.sitola.stanko.sage2.PointParserServer.entities.PointObject;
import cz.muni.fi.sitola.stanko.sage2.PointParserServer.parsers.Parser;
import cz.muni.fi.sitola.stanko.sage2.PointParserServer.parsers.ParserImpl;
import cz.muni.fi.sitola.stanko.sage2.PointParserServer.server.WebSocketServer;

import java.io.*;
import java.util.Set;

/**
 * @author stanko
 * @date 3/2/15.
 */
public class ParserThread implements Runnable {

    private ApplicationSettings settings = ApplicationSettings.getInstance();


    @Override
    public void run() {
        System.out.println("*** Running parser....");
        Parser parser = null;
        parser = new ParserImpl(System.in);


        while(!parser.isEof())
        {
            try {
                Set<PointObject> objects = parser.parseSection();
                if(objects == null) continue;
                System.out.println("ParsedObject: " + objects);
                System.out.println("Alive: " + parser.getAliveQueue().toString());
                System.out.println();

                for(PointObject object : objects)
                {
                    WebSocketServer.sendMessage("P " + object.getJSON());
                }

//                WebSocketServer.sendMessage("P " + objects.getJSON());
                WebSocketServer.sendMessage("Q " + parser.getAliveQueue().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
