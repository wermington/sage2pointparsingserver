package cz.muni.fi.sitola.stanko.sage2.PointParserServer.server;

import javax.servlet.annotation.WebServlet;

import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
@SuppressWarnings("serial")
@WebServlet(name = "PointParserServer", urlPatterns = {"/pointParser"})
public class MyServlet extends WebSocketServlet
{
    @Override
    public void configure(WebSocketServletFactory factory) {
        factory.register(WebSocketServer.class);
    }
}

