package cz.muni.fi.sitola.stanko.sage2.PointParserServer.app;

import cz.muni.fi.sitola.stanko.sage2.PointParserServer.server.MyServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author stanko
 * @date 3/2/15.
 */
public class ApplicationEngine {

    private static boolean isServerRunning= false;
    private static boolean isApplicationInit = false;

    private static ApplicationSettings settings = new ApplicationSettings();

    public static void init(String[] args)
    {
        parseArgs(args);
        isApplicationInit = true;
    }

    private static void parseArgs(String[] args) {

    }


    public static void runWebServer2()
    {
        Server server = new Server(settings.getWebsocketPortNumber());
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);
        context.addServlet(new ServletHolder(new MyServlet()), "/pointParser");
        try
        {
            server.start();
           // server.join();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public static void start(String[] args)
    {
        runConfig(args);
        runWebServer2();
        runParser();
    }

    private static void runConfig(String[] args) {
        try{
            SettingsLoader.load();

        }catch (IOException ex)
        {
            System.out.println("Writing default config file.");
            ex.printStackTrace();


            try {

                SettingsLoader.save();


            }catch (Exception saveex)
            {
                System.err.println("Cannot save file");
                saveex.printStackTrace();
                System.exit(1);
            }

            try {
                SettingsLoader.load();
            }catch (Exception load_error)
            {
                load_error.printStackTrace();
                System.exit(2);
            }


        }

        try {
            if (args.length == 2) {
                settings.setResWidth(Integer.parseInt(args[0]));
                settings.setResHeight(Integer.parseInt(args[1]));
            } else if (args.length == 3) {
                settings.setResWidth(Integer.parseInt(args[0]));
                settings.setResHeight(Integer.parseInt(args[1]));
                settings.setWebsocketPortNumber(Integer.parseInt(args[2]));
            } else if (args.length == 1){
                settings.setWebsocketPortNumber(Integer.parseInt(args[2]));
            }


        } catch(NumberFormatException ex)
        {
            ex.printStackTrace();
        }

    }

    private static void runParser() {
        ParserThread parser= new ParserThread();
        Thread parserThread = new Thread(parser);
        parserThread.start();
    }


    // Application EntryMethod
    public static void main(String[] args) {
        ApplicationEngine.start(args);
    }




}
