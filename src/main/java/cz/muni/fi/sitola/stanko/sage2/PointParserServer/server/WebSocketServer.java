package cz.muni.fi.sitola.stanko.sage2.PointParserServer.server;

import cz.muni.fi.sitola.stanko.sage2.PointParserServer.app.ApplicationSettings;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;


/**
 * @author stanko
 * @date 3/2/15.
 */
@WebSocket
public class WebSocketServer  {

    private static Set<Session> peers = Collections.synchronizedSet(new CopyOnWriteArraySet<Session>());
    private static ApplicationSettings settings = ApplicationSettings.getInstance();

    @OnWebSocketConnect
    public void onOpen (Session peer) {
        System.out.println("Connection established: " + peer.getRemoteAddress().toString());
        peers.add(peer);

        try {
            peer.getRemote().sendString("Connected to server...");
            sendMessage("RW: " + settings.getResWidth());
            sendMessage("RH: " + settings.getResHeight());
        } catch (IOException e) {
            System.out.println(e);
        }

    }

    @OnWebSocketMessage
    public void onMessage(String message) {
        System.out.println("Message: " + message);
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        System.out.println("Close: statusCode=" + statusCode + ", reason=" + reason);
    }


    @OnWebSocketError
    public void onError(Throwable t) {
        System.err.println("Error: " + t.getMessage());
    }

    public static void sendMessage(String message)
    {
        Iterator iter = peers.iterator();
        while(iter.hasNext())
        {
            Session peer = (Session) iter.next();
            try {
                if(peer.isOpen())
                    peer.getRemote().sendString(message);
            } catch (IOException e) {
                //e.printStackTrace();
                peer.close();
                iter.remove();
            }
        }
    }

}
