package cz.muni.fi.sitola.stanko.sage2.PointParserServer.parsers;

import cz.muni.fi.sitola.stanko.sage2.PointParserServer.entities.PointObject;

import java.io.IOException;
import java.util.Set;

/**
 * @author stanko
 * @date 3/2/15.
 */
public class ParsersDemo {
    public static void main(String[] args) {
        Parser parser = new ParserImpl();
        while(!parser.isEof())
        {
            try {
                Set<PointObject> object = parser.parseSection();
                if(object == null) continue;
                System.out.println("ParsedObject: " + object);
                System.out.println("Alive: " + parser.getAliveQueue().toString());
                System.out.println();
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }
        }



    }

}
