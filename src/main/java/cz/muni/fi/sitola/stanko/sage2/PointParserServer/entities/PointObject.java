package cz.muni.fi.sitola.stanko.sage2.PointParserServer.entities;

import org.json.JSONObject;

/**
 * @author stanko
 * @date 3/2/15.
 */
public class PointObject {
    private double x;
    private double y;
    private double z;
    private long id;


    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }


    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return this.id;
    }


    @Override
    public String toString() {
        return "PointObject{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                ", id=" + id +
                '}';
    }

    public String getOutput() {
        return id + ";" + x + ";" + y + ";" + z;
    }


    public String getJSON()
    {
        JSONObject object = new JSONObject();
        object.put("x", x);
        object.put("y", y);
        object.put("z", z);
        object.put("id", id);
        return object.toString();
    }


}
