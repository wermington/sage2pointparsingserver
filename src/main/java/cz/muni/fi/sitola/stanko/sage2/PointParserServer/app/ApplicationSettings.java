package cz.muni.fi.sitola.stanko.sage2.PointParserServer.app;

/**
 * Created by wermington on 14.3.2015.
 */
public class ApplicationSettings {

    private static ApplicationSettings INSTANCE = new ApplicationSettings();

    public static ApplicationSettings getInstance() { return INSTANCE;}


    private int websocketPortNumber = 10888;
    private int resWidth = 4000;
    private int resHeight = 2600;


    public int getWebsocketPortNumber() {
        return websocketPortNumber;
    }

    public void setWebsocketPortNumber(int websocketPortNumber) {
        this.websocketPortNumber = websocketPortNumber;
    }

    public int getResHeight() {
        return resHeight;
    }

    public void setResHeight(int resHeight) {
        this.resHeight = resHeight;
    }

    public int getResWidth() {
        return resWidth;
    }

    public void setResWidth(int resWidth) {
        this.resWidth = resWidth;
    }


}
