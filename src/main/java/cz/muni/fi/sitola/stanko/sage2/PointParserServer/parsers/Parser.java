package cz.muni.fi.sitola.stanko.sage2.PointParserServer.parsers;

import cz.muni.fi.sitola.stanko.sage2.PointParserServer.entities.PointObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

/**
 * @author stanko
 * @date 3/2/15.
 */
public interface Parser {

    void setInputStream(InputStream inputStream);
    Set<PointObject> parseSection() throws IOException;

    boolean isEof();

    Set<PointObject> getParsedObjects();
    Set<Long> getAliveQueue();
}
