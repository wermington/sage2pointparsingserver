package cz.muni.fi.sitola.stanko.sage2.PointParserServer.app;


import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;

/**
 * Created by wermington on 14.3.2015.
 */
public class SettingsLoader {

    private static ApplicationSettings settings = ApplicationSettings.getInstance();

    //public static final String DEFAULT_CONFIG_DIR = System.getProperty("user.home") + "/.config/pointParser/";
    public static final String DEFAULT_CONFIG_DIR = "";
    public static final String DEFAULT_CONFIG_PATH = DEFAULT_CONFIG_DIR + "main.json";

    public static void load(String filename) throws IOException {

        JSONTokener tokener;
        FileReader reader = new FileReader(filename);
        tokener = new JSONTokener(reader);



        JSONObject jsonObject = new JSONObject(tokener);

        settings.setResHeight(jsonObject.getInt("resolutionHeight"));
        settings.setResWidth(jsonObject.getInt("resolutionWidth"));
        settings.setWebsocketPortNumber(jsonObject.getInt("websocketPortNumber"));
        reader.close();
    }


    public static void save(String filename) throws IOException {
        JSONObject jsonObject = new JSONObject();
        new File(DEFAULT_CONFIG_DIR).mkdirs();
        jsonObject.put("resolutionHeight", settings.getResHeight());
        jsonObject.put("resolutionWidth", settings.getResWidth());
        jsonObject.put("websocketPortNumber", settings.getWebsocketPortNumber());

        FileWriter fw = new FileWriter(filename);
        String js_string = jsonObject.toString(2);
        fw.write(js_string);
        fw.close();
    }


    public static void save() throws IOException {
        save(DEFAULT_CONFIG_PATH);
    }

    public static void load() throws IOException
    {
        load(DEFAULT_CONFIG_PATH);
    }


}
