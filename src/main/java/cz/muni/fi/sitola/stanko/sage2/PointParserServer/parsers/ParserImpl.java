package cz.muni.fi.sitola.stanko.sage2.PointParserServer.parsers;

import cz.muni.fi.sitola.stanko.sage2.PointParserServer.entities.PointObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

/**
 * @author stanko
 * @date 3/2/15.
 */
public class ParserImpl implements Parser {

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private Set<PointObject> parsedObjects = null;
    private Set<Long> aliveObjects = null;
    private boolean eof = false;


    public ParserImpl()
    {

    }


    public ParserImpl(InputStream is)
    {
        setInputStream(is);
    }

    public void setInputStream(InputStream inputStream)

    {
        reader = new BufferedReader(new InputStreamReader(inputStream));
    }


    /**
     * Parsing block between new lines
     * @return Null if not in spec. format, Object if parsed correctly
     * @throws IOException - If not in specified format or cannot read stream
     */
    public Set<PointObject> parseSection() throws IOException {

        Set<PointObject> objects = new HashSet<PointObject>();
        String line = "";
        while ((line = reader.readLine()) != null)
        {
            // If read empty line break cycle and return object
            if(line.trim().length() == 0) return null;


            String[] splitLine = line.split(" "); // split by space
            if(splitLine.length == 1)
                throw new IOException("NOT RECOGNISED FORMAT");

            // read dta
            if(splitLine[0].endsWith("dta"))
            {
                // DO NOTHING
            }
            // read pointer
            else if(splitLine[0].endsWith("pointer"))
            {
                // DO POINTER
                PointObject object = new PointObject();

                // GET CID
                String[] splitCid = splitLine[1].split("=");
                if(splitCid.length == 1)
                    throw new IOException("NOT RECOGNISED FORMAT");

                object.setId(Long.parseLong(splitCid[1]));

                // GET POINT
                String[] parsePos = splitLine[2].split("=");
                if(parsePos.length == 1)
                    throw new IOException("NOT RECOGNISED FORMAT");
                String[] coordinates = parsePos[1].substring(1, parsePos[1].length() - 1).split(";");
                if(coordinates.length == 1)
                    throw  new IOException("NOT RECOGNISED FORMAT");
                object.setX(Double.parseDouble(coordinates[0]));
                object.setY(Double.parseDouble(coordinates[1]));
                object.setZ(Double.parseDouble(coordinates[2]));

                objects.add(object);
                parsedObjects = objects;


            }else if(splitLine[0].endsWith("alive"))
            {
                // DO ALIVE

                Set<Long> alive = parseAlive(line);

                // TODO
                this.aliveObjects = alive;
                break;
            }

        }
        if(line == null)
            eof = true;

        if(objects.size() == 0) return null;
        return objects;
    }


    /**
     * Parsing set of alive objects
     * @param line - line begins with alive
     * @return - null if empty; Set of id's if not
     */
    private Set<Long> parseAlive(String line ) {
        Set<Long> alive = new HashSet<Long>();

        // Get all ids from set
        String ids_set = line.split(" ", 2)[1].split("=")[1];
        String s_numbers = ids_set.split("}")[0].substring(1, ids_set.length() - 1);

        // Case when ids is empty => there is not alive object
        if (s_numbers.trim().length() == 0) return null;
        String[] numbers = s_numbers.trim().split(" ");

        // Iterate through all parsed numbers from set
        for (String num : numbers) {

            // Try parse integer from id set
            long id = Long.parseLong(num);
            alive.add(id);
        }

        return alive;
    }

    public Set<PointObject> getParsedObjects() {
        return parsedObjects;
    }

    public Set<Long> getAliveQueue() {
        return aliveObjects;
    }


    public boolean isEof() {
        return eof;
    }
}
